package com.example.parrot.pong1;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameEngine extends SurfaceView implements Runnable {

    // -----------------------------------
    // ## ANDROID DEBUG VARIABLES
    // -----------------------------------

    // Android debug variables
    final static String TAG="PONG-GAME";

    // -----------------------------------
    // ## SCREEN & DRAWING SETUP VARIABLES
    // -----------------------------------

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;

    int ballXposition;
    int ballYPosition;

    int racketXPosition;
    int racketYPosition;
    int score = 0;

    String personTapped = "";



    // -----------------------------------
    // ## GAME SPECIFIC VARIABLES
    // -----------------------------------

    // ----------------------------
    // ## SPRITES
    // ----------------------------

    // represents the (x,y) position of the ball
    Point ballPosition;


    // ----------------------------
    // ## GAME STATS - number of lives, score, etc
    // ----------------------------


    public GameEngine(Context context, int w, int h) {
        super(context);


        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;


        this.printScreenInfo();

        // @TODO: Add your sprites to this section
        // This is optional. Use it to:
        //  - setup or configure your sprites
        //  - set the initial position of your sprites


        // set the initial position of the ball to be middle of screen
        ballPosition = new Point();
        ballPosition.x = this.screenWidth / 2;
        ballPosition.y = this.screenHeight / 2;

        this.ballXposition = this.screenWidth/2;
        this.ballYPosition = this.screenHeight/2;

        this.racketXPosition = 500;
        this.racketYPosition = 1500;


        // @TODO: Any other game setup stuff goes here


    }

    // ------------------------------
    // HELPER FUNCTIONS
    // ------------------------------

    // This function prints the screen height & width to the screen.
    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }


    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    String directionballIsMoving = "top";

    // 1. Tell Android the (x,y) positions of your sprites
    public void updatePositions() {
        // @TODO: Update the position of the sprites

        if(directionballIsMoving.contentEquals("top")){
            this.ballYPosition = this.ballYPosition - 10;

            //If the ball hits ceiling change direction
            if (this.ballYPosition <= 0){
                directionballIsMoving = "down";
            }
        }else if(directionballIsMoving.contentEquals("down")){
            this.ballYPosition = this.ballYPosition + 10;

            //If the ball hits bottom change direction
            if (this.ballYPosition >= this.screenHeight - 250){
                directionballIsMoving = "top";
            }
        }

        // calculate the racket's new position
                if (personTapped.contentEquals("right")){
                       this.racketXPosition = this.racketXPosition + 10;
                    }
                else if (personTapped.contentEquals("left")){
                        this.racketXPosition = this.racketXPosition - 10;
                    }

        // @TODO: Collision detection code

        if (ballYPosition >= this.racketYPosition){
            directionballIsMoving = "top";

            //increase the game score
            this.score = this.score + 1;

        }

    }

    // 2. Tell Android to DRAW the sprites at their positions
    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------
            // Put all your drawing code in this section

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,0,0,255));
            paintbrush.setColor(Color.WHITE);


            //@TODO: Draw the sprites (rectangle, circle, etc)
            // 1.Draw the ball

            this.canvas.drawRect(
                    ballXposition,
                    ballYPosition,
                    ballXposition + 50,
                    ballYPosition + 50,
                    paintbrush
            );

            //2.Draw the racket
            paintbrush.setColor(Color.YELLOW);
            this.canvas.drawRect(racketXPosition,racketYPosition,racketXPosition + 400,racketYPosition +50,paintbrush);
            paintbrush.setColor(Color.WHITE);

            //@TODO: Draw game statistics (lives, score, etc)
            paintbrush.setTextSize(60);
            canvas.drawText("Score: " + this.score, 20, 100, paintbrush);

            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    // Sets the frame rate of the game
    public void setFPS() {
        try {
            gameThread.sleep(50);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {
            // user pushed down on screen

            //1. Get position of the tap
            float fingerXPosition = event.getX();
            float fingerYPosition = event.getY();

            //2. Compare position of tap with the middle of screen

            int middleOfScreen = this.screenWidth/2;
            if (fingerXPosition <= middleOfScreen){
                personTapped = "left";
            }else if (fingerXPosition > middleOfScreen){
                personTapped = "right";
            }

        }
        else if (userAction == MotionEvent.ACTION_UP) {
            // user lifted their finger
        }
        return true;
    }
}